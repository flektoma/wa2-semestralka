package hello.card;

import hello.payment.Payment;
import hello.user.User;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Tomáš on 22.05.2016.
 */
@Entity
public class Card {

    @Id
    @GeneratedValue
    private int id;

    private int number;

    private int balance;

    @ManyToOne
    private User userId;

    @OneToMany(mappedBy = "cardId")
    private List<Payment> payments;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }
}
