package hello.card;

import hello.payment.Payment;
import hello.user.User;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Tomáš on 22.05.2016.
 */

public class CardDTO {


    private int id;

    private int number;

    private int balance;

    private int userId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
