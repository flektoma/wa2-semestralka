package hello.card;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Tomáš on 22.05.2016.
 */
public interface CardRepository extends JpaRepository<Card, Integer> {

}
