package hello.history;

import hello.user.User;

import javax.persistence.*;

/**
 * Created by Tomáš on 23.05.2016.
 */
@Entity
public class History {

    @Id
    @GeneratedValue
    private int id;

    @OneToOne
    private User userId;

    @Lob
    @Column( length = 100000 )
    private String history;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }
}
