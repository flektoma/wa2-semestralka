package hello.history;

import hello.RabbitConfiguration;
import hello.user.User;
import hello.user.UserDTO;
import hello.user.UserRepository;
import hello.vote.Vote;
import hello.vote.VoteDTO;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Tomáš on 23.05.2016.
 */
@Controller
public class HistoryController {

    ApplicationContext context =
            new AnnotationConfigApplicationContext(RabbitConfiguration.class);
    AmqpTemplate rabbitTemplate = context.getBean(AmqpTemplate.class);

    @Autowired
    HistoryRespository historyRespository;

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public String historyForm(Model model){

        model.addAttribute("history", new History());
        model.addAttribute("users", userRepository.findAll());

        return "history";
    }

    @RequestMapping(value = "/history", method = RequestMethod.POST)
    public String requestHistory(@ModelAttribute HistoryDTO historyDTO , Model model){

        User user = userRepository.getOne(historyDTO.getUserId());
        History history = historyRespository.findByUserId(user);

        if (history != null){
            //deleting previous history
            historyRespository.delete(history);
        }

        rabbitTemplate.convertAndSend("myqueue", historyDTO.getUserId());


        model.addAttribute("user", user);

        return "history_requested";
    }

    @RequestMapping(value = "/history/result", method = RequestMethod.GET)
    public String requestHistoryResult(@RequestParam(value="userId", required=true) Integer id, Model model){

        User user = userRepository.getOne(id);

        History history = historyRespository.findByUserId(user);

        if (history == null){
            history = new History();
            history.setHistory("Your request is being processed .....");
        }
        model.addAttribute("user", user);
        model.addAttribute("history", history);

        return "history_result";
    }

    @RequestMapping(value = "/history/pending", method = RequestMethod.GET)
    public String historyFormPending(Model model){

        model.addAttribute("history", new History());
        model.addAttribute("users", userRepository.findAll());

        return "history_pending";
    }

    @RequestMapping(value = "/history/pending", method = RequestMethod.POST)
    public ModelAndView displayHistoryPending(@ModelAttribute HistoryDTO historyDTO , Model model){

        return new ModelAndView("redirect:" + "/history/result?userId=" + historyDTO.getUserId());


    }

//    @RequestMapping(value = "/history/pending", method = RequestMethod.POST)
//    public String displayHistoryPending(@ModelAttribute HistoryDTO historyDTO , Model model){
//
//        return requestHistoryResult(historyDTO.getUserId(), model);
//
//
//    }
}
