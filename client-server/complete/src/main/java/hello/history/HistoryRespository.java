package hello.history;

import hello.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * Created by Tomáš on 23.05.2016.
 */
public interface HistoryRespository extends JpaRepository<History, Integer> {

    History findByUserId(User userId);
}
