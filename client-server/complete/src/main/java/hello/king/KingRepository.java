package hello.king;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Tomáš on 22.05.2016.
 */
public interface KingRepository extends JpaRepository<King, Integer> {
}
