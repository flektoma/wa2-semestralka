package hello.payment;

import hello.card.Card;
import hello.item.Item;
import hello.user.User;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Tomáš on 22.05.2016.
 */
@Entity
public class Payment {

    @Id
    @GeneratedValue
    private int id;

    private Date time;

    private int sum;

    @ManyToOne
    private User userId;

    @ManyToOne
    private Card cardId;

    @ManyToMany()
    private List<Item> items;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Card getCardId() {
        return cardId;
    }

    public void setCardId(Card cardId) {
        this.cardId = cardId;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
