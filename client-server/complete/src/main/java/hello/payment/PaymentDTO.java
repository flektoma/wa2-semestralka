package hello.payment;

import hello.card.Card;
import hello.item.Item;
import hello.user.User;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Tomáš on 22.05.2016.
 */

public class PaymentDTO {

    private int id;

    private Date time;

    private int sum;

    private int userId;

    private int cardId;

    private List<Integer> items;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public List<Integer> getItems() {
        return items;
    }

    public void setItems(List<Integer> items) {
        this.items = items;
    }
}
