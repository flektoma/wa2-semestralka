package hello.payment;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Tomáš on 22.05.2016.
 */
public interface PaymentRepository extends JpaRepository<Payment, Integer> {
}
