package hello.user;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.jws.soap.SOAPBinding;

/**
 * Created by Tomáš on 22.05.2016.
 */
public interface UserRepository extends JpaRepository<User, Integer> {


}
