package hello.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;

/**
 * Created by Tomáš on 21.05.2016.
 */
@Controller
public class AddressController {

    @Autowired
    private AddressRepository addressRepository;

    @RequestMapping(value = "/address", method = RequestMethod.GET)
    public String addressForm(Model model){
        model.addAttribute("address", new Address());
        return "address";
    }

    @RequestMapping(value = "/address", method = RequestMethod.POST)
    public String saveAddress(@ModelAttribute Address address, Model model) {

        //model.addAttribute("name", name);

//        Address address = new Address();
//        address.setStreet(street);
//        address.setCity(city);
//        address.setNumber(number);

        addressRepository.save(address);

        return "success";
    }





}