package hello.card;

import hello.address.AddressRepository;
import hello.user.User;
import hello.user.UserDTO;
import hello.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Tomáš on 22.05.2016.
 */
@Controller
public class CardController {

    @Autowired
    CardRepository cardRepository;

    @Autowired
    UserRepository userRepository;


    @RequestMapping(value = "/card", method = RequestMethod.GET)
    public String cardForm(Model model){

        model.addAttribute("card", new Card());
        model.addAttribute("users", userRepository.findAll());

        return "card";
    }

    @RequestMapping(value = "/card", method = RequestMethod.POST)
    public String saveCard(@ModelAttribute CardDTO cardDTO, Model model){

        Card card = new Card();
        card.setNumber(cardDTO.getNumber());
        card.setBalance(cardDTO.getBalance());
        card.setUserId(userRepository.getOne(cardDTO.getUserId()));

        cardRepository.save(card);

        return "success";
    }

    @RequestMapping(value = "/card/money", method = RequestMethod.GET)
    public String cardFormMoney(Model model){

        model.addAttribute("card", new Card());
        model.addAttribute("cards", cardRepository.findAll());

        return "card_money";
    }

    @RequestMapping(value = "/card/money", method = RequestMethod.POST)
    public String updateCardMoney(@ModelAttribute CardDTO cardDTO, Model model){

        Card card = cardRepository.getOne(cardDTO.getId());
        if (cardDTO.getBalance() < 0) return "negative_balance";
        card.setBalance(card.getBalance() + cardDTO.getBalance());
        cardRepository.save(card);

        return "money_added";
    }
}
