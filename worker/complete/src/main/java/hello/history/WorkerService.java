package hello.history;

import hello.item.Item;
import hello.payment.Payment;
import hello.payment.PaymentRepository;
import hello.user.User;
import hello.user.UserRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Tomáš on 23.05.2016.
 */
@Component
public class WorkerService {

    @Autowired
    HistoryRespository historyRespository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @RabbitListener(queues = "myqueue")
    @Transactional
    public void processRequest(String userId) throws InterruptedException {


        Integer id = Integer.valueOf(userId);
        User user = userRepository.findOne(id);

        History history = historyRespository.findByUserId(user);
        if (history == null){
            history = new History();
        }

        List<Payment> payments = paymentRepository.findByUserId(user);

        StringBuilder sb = new StringBuilder();
        for (Payment payment : payments) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM. HH:mm");
            sb.append("-------------------------------------------------" + System.getProperty("line.separator"));
            sb.append("Time: " + sdf.format(payment.getTime()) + System.getProperty("line.separator"));
            sb.append("Payment with card: " + payment.getCardId().getNumber() + System.getProperty("line.separator"));
            for (Item item : payment.getItems()) {
                sb.append(item.getName() + " " + item.getCost() + " Kc" + System.getProperty("line.separator"));
            }
        }

        history.setUserId(user);
        history.setHistory(sb.toString());

        Thread.sleep(10 * 1000);


        historyRespository.save(history);
    }

}
