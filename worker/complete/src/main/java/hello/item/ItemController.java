package hello.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Tomáš on 23.05.2016.
 */
@Controller
public class ItemController {

    @Autowired
    ItemRepository itemRepository;

    @RequestMapping(value = "/item", method = RequestMethod.GET)
    public String itemForm(Model model){

        model.addAttribute("item", new Item());

        return "item";
    }

    @RequestMapping(value = "/item", method = RequestMethod.POST)
    public String addItem(@ModelAttribute Item item, Model model){

        itemRepository.save(item);

        return "success";
    }
}
