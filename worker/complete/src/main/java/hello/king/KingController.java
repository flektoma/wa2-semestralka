package hello.king;

import hello.item.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Tomáš on 23.05.2016.
 */
@Controller
public class KingController {

    @Autowired
    KingRepository kingRepository;

    @RequestMapping(value = "/king", method = RequestMethod.GET)
    public String kingForm(Model model){

        model.addAttribute("king", new King());

        return "king";
    }

    @RequestMapping(value = "/king", method = RequestMethod.POST)
    public String addKing(@ModelAttribute King king, Model model){

        kingRepository.save(king);

        return "success";
    }
}
