package hello.payment;

import hello.card.Card;
import hello.card.CardRepository;
import hello.item.Item;
import hello.item.ItemRepository;
import hello.king.KingRepository;
import hello.user.UserDTO;
import hello.user.UserRepository;
import hello.vote.Vote;
import hello.vote.VoteDTO;
import hello.vote.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tomáš on 23.05.2016.
 */
@Controller
public class PaymentController {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/payment", method = RequestMethod.GET)
    public String paymentForm(Model model){

        model.addAttribute("payment", new Payment());
        model.addAttribute("cards", cardRepository.findAll());
        model.addAttribute("items", itemRepository.findAll());
        //model.addAttribute("users", userRepository.findAll());

        return "payment";
    }

    @RequestMapping(value = "/payment", method = RequestMethod.POST)
    public String addPayment(@ModelAttribute PaymentDTO paymentDTO, Model model){

        Payment payment = new Payment();
        Card card = cardRepository.getOne(paymentDTO.getCardId());
        payment.setCardId(card);
        payment.setUserId(card.getUserId());
        payment.setTime(new Date());

        int sum = 0;
        List<Item> itemList = new LinkedList<>();
        for (Integer id : paymentDTO.getItems()) {
            Item item = itemRepository.getOne(id);
            sum += item.getCost();
            itemList.add(item);
        }
        if (sum > card.getBalance()) return "money";

        payment.setSum(sum);
        payment.setItems(itemList);
        card.setBalance(card.getBalance()-sum);

        cardRepository.save(card);

        paymentRepository.save(payment);

        return "purchase";
    }

}
