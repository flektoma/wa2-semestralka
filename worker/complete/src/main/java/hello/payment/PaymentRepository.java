package hello.payment;

import hello.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.jws.soap.SOAPBinding;
import java.util.List;

/**
 * Created by Tomáš on 22.05.2016.
 */
public interface PaymentRepository extends JpaRepository<Payment, Integer> {

    List<Payment> findByUserId(User user);

}
