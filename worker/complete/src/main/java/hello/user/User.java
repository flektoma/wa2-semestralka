package hello.user;

import hello.address.Address;
import hello.card.Card;
import hello.history.History;
import hello.payment.Payment;
import hello.vote.Vote;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Tomáš on 22.05.2016.
 */
@Entity
public class User {

    @Id
    @GeneratedValue
    private int id;

    private String name;
    private String phone;

    @ManyToOne
    private Address addressId;

    @OneToMany(mappedBy = "userId")
    private List<Card> cards;

    @OneToMany(mappedBy = "userId")
    private List<Payment> payments;

    @OneToOne
    private Vote voteId;

    @OneToOne
    private History historyId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Address getAddressId() {
        return addressId;
    }

    public void setAddressId(Address addressId) {
        this.addressId = addressId;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public Vote getVoteId() {
        return voteId;
    }

    public void setVoteId(Vote voteId) {
        this.voteId = voteId;
    }

    public History getHistoryId() {
        return historyId;
    }

    public void setHistoryId(History historyId) {
        this.historyId = historyId;
    }
}
