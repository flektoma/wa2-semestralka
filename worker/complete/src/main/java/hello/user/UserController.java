package hello.user;

import hello.address.Address;
import hello.address.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.jws.soap.SOAPBinding;
import java.util.List;

/**
 * Created by Tomáš on 23.05.2016.
 */
@Controller
public class UserController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    AddressRepository addressRepository;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String userForm(Model model){

        model.addAttribute("user", new User());
        model.addAttribute("addresses", addressRepository.findAll());

        return "user";
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String saveUser(@ModelAttribute UserDTO userDTO, Model model){

        User user = new User();
        user.setName(userDTO.getName());
        user.setPhone(userDTO.getPhone());
        user.setAddressId(addressRepository.getOne(userDTO.getAddressId()));

        userRepository.save(user);

        return "success";

    }

}
