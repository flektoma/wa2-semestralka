package hello.user;

import hello.address.Address;
import hello.card.Card;
import hello.payment.Payment;
import hello.vote.Vote;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Tomáš on 22.05.2016.
 */
public class UserDTO {

    private int id;

    private String name;
    private String phone;
    private int addressId;
    private Vote voteId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public Vote getVoteId() {
        return voteId;
    }

    public void setVoteId(Vote voteId) {
        this.voteId = voteId;
    }
}
