package hello.vote;

import hello.king.King;
import hello.user.User;

import javax.persistence.*;

/**
 * Created by Tomáš on 22.05.2016.
 */
@Entity
public class Vote {

    @Id
    @GeneratedValue
    private int id;

    @OneToOne
    private User userId;

    @ManyToOne
    private King kingId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public King getKingId() {
        return kingId;
    }

    public void setKingId(King kingId) {
        this.kingId = kingId;
    }
}
