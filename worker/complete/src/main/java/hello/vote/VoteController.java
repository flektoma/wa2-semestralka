package hello.vote;

import hello.item.Item;
import hello.king.KingRepository;
import hello.user.User;
import hello.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.jws.soap.SOAPBinding;

/**
 * Created by Tomáš on 23.05.2016.
 */
@Controller
public class VoteController {

    @Autowired
    VoteRepository voteRepository;

    @Autowired
    KingRepository kingRepository;

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/vote", method = RequestMethod.GET)
    public String voteForm(Model model){

        model.addAttribute("vote", new Vote());
        model.addAttribute("kings", kingRepository.findAll());
        model.addAttribute("users", userRepository.findAll());

        return "vote";
    }

    @RequestMapping(value = "/vote", method = RequestMethod.POST)
    public String addVote(@ModelAttribute VoteDTO voteDTO, Model model){

        Vote vote = new Vote();
        vote.setKingId(kingRepository.getOne(voteDTO.getKingId()));
        User user = userRepository.getOne(voteDTO.getUserId());
        if (user.getVoteId() != null) return "already_voted";
        vote.setUserId(user);
        user.setVoteId(vote);

        voteRepository.save(vote);
        userRepository.save(user);

        return "voted";
    }

}
