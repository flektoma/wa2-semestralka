package hello.vote;

import hello.king.King;
import hello.user.User;

import javax.persistence.*;

/**
 * Created by Tomáš on 22.05.2016.
 */

public class VoteDTO {
    private int id;

    private int userId;

    private int kingId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getKingId() {
        return kingId;
    }

    public void setKingId(int kingId) {
        this.kingId = kingId;
    }
}
