package hello.vote;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Tomáš on 22.05.2016.
 */
public interface VoteRepository extends JpaRepository<Vote, Integer> {
}
